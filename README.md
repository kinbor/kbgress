# kbgress

#### 介绍
它是kbnet开发框架的基础项目，为程序开发提供基础保障，如权限控制、令牌颁发和验证、网络安全、分布式计算和存储、工具类库等等。

#### 软件架构
kbgress分为kbgress.client、kbgress.server和kbgress三个程序总成，其中kbgress.client和kbgress.server主要用作基于kbnet框架的程序开发，kbgress则是通过web方式管理和维护自身。


#### 安装教程
1.  首先下载kbgress，然后安装数据库，最后按照步骤安装kbgress.
2.  在基于kbgress进行开发以前，请启动kbgress.server，因为kbgress.client会于它通讯，获取程序运行必要的数据和参数。
3.  鉴于第二条信息，在调试测试基于kbnet框架和kbgress.client程序时，一定要将程序需要的数据和参数配置完整。

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
